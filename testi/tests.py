from django.test import TestCase, Client
from . import views
from .models import Testimoni
from .forms import *
from django.urls import reverse


class testimoniTest(TestCase):
    def test_url(self):
        c = Client()
        response = c.get('/testimoni/')
        self.assertTrue(response.status_code, 200)

    def test_template(self):
        response = Client().get('/testimoni/')
        self.assertTemplateUsed(response, 'testimoni.html')
    
    def test_create_testimoni(self):
        name = "naura"
        review = "keren banget kak"
        return Testimoni.objects.create(name = name, review = review)
    
    def test_testimoni(self):
        r = self.test_create_testimoni() 
        self.assertTrue(isinstance(r, Testimoni))
        self.assertEqual(r.__str__(), r.review)
        resp = self.client.get("/testimoni/")
        self.assertEqual(resp.status_code, 200) 
    
    def test_testi_cre(self):
        form_data = {"name" : "naura","review" : "keren banget kak"}
        form = ReviewForm(data = form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/testimoni/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/testimoni/')
        self.assertEqual(response.status_code, 200)
    
    





# Create your tests here.
