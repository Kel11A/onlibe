from django.urls import path
from . import views
from django.conf.urls import url

app_name = 'testi'

urlpatterns = [
    path('', views.testimoni, name='testimoni'),
]