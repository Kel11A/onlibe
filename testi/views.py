from django.shortcuts import render
from django.shortcuts import redirect
from .models import Testimoni
from . import forms
from .forms import ReviewForm
# from django.contrib.auth.decorators import login_required

# @login_required(login_url="/accounts/login/")
def testimoni(request):
	testimonies = Testimoni.objects.all().values().order_by('name')
	if (request.method == 'POST'):
		form = ReviewForm(request.POST)
		if (form.is_valid()):
			form.save()
			return redirect('/testimoni')
	else:
		form = ReviewForm()
	return render(request, 'testimoni.html', {'testimonies': testimonies, 'form':form})





