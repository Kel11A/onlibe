Tugas Kelompok 1

[![pipeline status](https://gitlab.com/Kel11A/onlibe/badges/master/pipeline.svg)](https://gitlab.com/Kel11A/onlibe/commits/master)

Kelas : A
Kelompok : 11

Nama Anggota :
1. Audilla Putri Ferialdi / 1806186755
2. Daniel Martin / 1806204921
3. Naura Azka Hanifa / 1806186780
4. Primo Giancarlo Uneputty /1806205180

Ide :
Website yang ingin kami ajukan berbentuk perpustakaan Online yang bernama onLibe. OnLibe hadir sebagai platform untuk mempermudah pencarian peminjaman buku bagi semua orang.

Link Heroku : https://onlibe.herokuapp.com/

Fitur :
- Login / Register
- Book List
- Add / Delete Book
- Borrow / Return Book
- FAQ / Testimoni / Help