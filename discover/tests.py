from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import*

# Create your tests here.
class test_Unit(TestCase) :
	def test_discover_url_is_exist(self):
		response = Client().get('/discover/')
		self.assertEqual(response.status_code,200)
		
	def test_discover_template_exist(self):
		response = Client().get('/discover/')
		self.assertTemplateUsed(response,'discover.html')
	
	def test_discover_file_render(self):
		found = resolve('/discover/')
		self.assertEqual(found.func, discover)

	def test_addbook_url_is_exist(self):
		response = Client().get('/discover/addbook/')
		self.assertEqual(response.status_code, 200)

	def test_addbook_template_exist(self):
		response = Client().get('/discover/addbook/')
		self.assertTemplateUsed(response, 'addBook.html')

	def test_addbook_file_render(self):
		found = resolve('/discover/addbook/')
		self.assertEqual(found.func, addBook)

	def test_create_addbook(self):
		title = "Matahari"
		author = "Tere Liye"
		publisher = "Gramedia"
		genre = "Fiksi"
		synopsis = "Bukunya bagus"
		return AddBook.objects.create(title = title, author = author, publisher = publisher,
			genre = genre, synopsis = synopsis)

	def test_addbook(self):
		r = self.test_create_addbook()
		self.assertTrue(isinstance(r, AddBook))
		self.assertEqual(r.__str__(), r.title)
		response = self.client.get('/discover/addbook/')
		self.assertEqual(response.status_code, 200)



