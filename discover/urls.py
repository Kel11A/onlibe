from django.urls import path
from django.contrib import admin
from . import views

app_name = 'discover'

urlpatterns = [
    path('', views.discover, name='discover'),
    path('addbook/', views.addBook, name='addbook')
]