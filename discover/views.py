from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import FormAddBook
from .models import AddBook

# Create your views here.
def addBook(request):
	form = FormAddBook()
	if request.method == "POST" :
		form = FormAddBook(request.POST)
		if form.is_valid():
			form.save()
		return redirect('discover:discover')
	return render(request, "addBook.html", {'form' : form})

def discover(request):
	kumpulanBuku = AddBook.objects.order_by("title")
	response = {
	'listBuku' : kumpulanBuku
	}
	return render(request, 'discover.html', response)