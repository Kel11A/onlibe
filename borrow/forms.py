from django import forms
from .models import Borrow
from django.forms.widgets import Select
import discover.models

class BorrowForm(forms.ModelForm):
    class Meta:
        CHOICES = discover.models.AddBook.objects.all()
        model = Borrow
        fields = ['title', 'borrowdate', 'returndate']
        widgets = {
            'title': Select(choices= ((x.title,x.title) for x in CHOICES)),
            'borrowdate': forms.DateInput(attrs={'type': 'date'}),
            'returndate': forms.DateInput(attrs={'type':'date'})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })