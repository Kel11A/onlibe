from django.contrib import admin

# Register your models here.
from .models import Borrow
from discover.models import AddBook
 
admin.site.register(Borrow)
