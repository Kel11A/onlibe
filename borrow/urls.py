from django.urls import path
from . import views

app_name = 'borrow'

urlpatterns = [
    path('profile/', views.profile, name='profile'),
    path('borrow/',views.borrow, name='borrow'),
   
]