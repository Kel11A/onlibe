from django.shortcuts import render, redirect
from .models import Borrow
from .forms import BorrowForm

# Create your views here.
def profile(request):
	bukupinjam = Borrow.objects.order_by("title")
	response = {
		'listBuku' : bukupinjam
	}
	return render (request, 'profile.html', response)
def borrow(request):
	if request.method == 'POST':
		form = BorrowForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/borrow/')
	else:
		form = BorrowForm()
	return render (request, 'book-borrow.html', {'form': form})

