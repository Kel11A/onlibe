from django.db import models
import discover.models

# Create your models here.
class Borrow(models.Model):
    arrchoice = []
    for obj in discover.models.AddBook.objects.all():
        arrchoice.append((obj.title, obj.title))

    title = models.CharField(max_length= 100)
    borrowdate = models.DateField()
    returndate = models.DateField()


    def __str__(self):
        return self.title