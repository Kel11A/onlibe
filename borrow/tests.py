from django.test import TestCase
from django.test import Client
from .models import Borrow
from . import views
from django.urls import reverse
from .forms import BorrowForm
from datetime import datetime, timezone
# Create your tests here.

class BorrowTest(TestCase):
    def test_url_borrow(self):
        c = Client()
        response = c.get('/borrow/')
        self.assertTrue(response.status_code,200)

    def test_url_profile(self):
        d = Client()
        response = d.get('/profile')
        self.assertTrue(response.status_code,200)

    def test_template_borrow(self):
        response = Client().get('/borrow/')
        self.assertTemplateUsed(response, 'book-borrow.html')

    def test_template_profile(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def make_a_borrow(self, judul = "buku bagus", pinjam="2019-12-12",
                      balik= "2019-12-15"):
        return Borrow.objects.create(title = judul, borrowdate = pinjam,
                                     returndate = balik)

    def testBorrow(self):
        f = self.make_a_borrow()
        self.assertTrue(isinstance(f, Borrow))
        self.assertEqual(f.__str__(), f.title)
        resp = self.client.get("/borrow/")
        self.assertEqual(resp.status_code, 200)


# Create your tests here.
