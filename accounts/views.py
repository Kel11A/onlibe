from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from .forms import UserRegisterForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def signup_view(request):
	if request.method == 'POST':
		form = UserRegisterForm(request.POST)
		if form.is_valid():
			username = form.cleaned_data.get('username')
			user = form.save()
			login(request, user)
			return redirect('homepage:index')
			# return redirect('test:testimoni')
	else:
		form = UserRegisterForm()
	return render(request, 'accounts/signup.html', {'form':form})

def login_view(request):
	if request.method == 'POST':
		form = AuthenticationForm(data=request.POST)
		if form.is_valid():
			user = form.get_user()
			login(request, user)
			return redirect('homepage:index')
			# return redirect('test:testimoni')
	else:
		form = AuthenticationForm()
	return render(request, 'accounts/login.html', {'form':form})

@login_required(login_url="/accounts/login/")
def logout_view(request):
	if request.method == 'POST':
		logout(request)
		return redirect('accounts:login')
	